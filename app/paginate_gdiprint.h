#ifndef _PAGINATE_GDIPRINT_H
#define _PAGINATE_GDIPRINT_H

typedef struct _Diagram Diagram;

void diagram_print_gdi(Diagram *dia);

#endif
